﻿/*
Copyright (c) 2013 Orich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ScriptDotNet;
using ScriptAPI;

namespace oRune
{
    class Program
    {
        static void ErrorExit(string ErrorText)
        {
            Console.WriteLine(ErrorText);
            Console.Write("Press any key to suicide.");
            Console.ReadKey(false);
            Environment.Exit(0);
        }

        static Container ChooseBag(string PromptText)
        {
            Console.WriteLine(PromptText);

            Item Result = Target.RequestTarget();

            if (Result == null || !Result.IsContainer)
            {
                Console.WriteLine("Invalid selection.  Use backpack.");
                return Self.Backpack;
            }

            return new Container(Result);
        }

        static GumpClass OpenRunebook(Item Runebook)
        {
            int NextGumpCount = GumpClass.GumpCount + 1, Attempts = 0;

            while (GumpClass.GumpCount <= NextGumpCount && ++Attempts < 5)
            {
                if (GumpClass.GumpCount > 0)
                    if (GumpClass.LastGump().ID == Runebook.ID)
                        return GumpClass.LastGump();

                Runebook.Use();
                Script.Wait(750);
            }

            return null; // return default(GumpClass);
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Press any key to start or SpaceBar for emptier");
            ConsoleKeyInfo key = Console.ReadKey();
            if (key.KeyChar == ' ')
            {
                Container BagOfBooksToEmpty = ChooseBag("Target your container of books to empty.");
                Container WhereRunesGo = ChooseBag("Target bag where you want me to move runes.");

                List<Item> BooksToEmpty = BagOfBooksToEmpty.Inventory(0x22c5, false);
                BagOfBooksToEmpty.Use();

                foreach (var book in BooksToEmpty)
                {
                    GumpClass EmptyGump = OpenRunebook(book);

                    var NumToEmpty = (from x in EmptyGump.XmfHtmlGump
                                      where x.Cliloc_ID == 1062722
                                      select x).Count() - (from g in EmptyGump.Textlines()
                                                           where g.Equals("Empty")
                                                           select g).Count();

                    for (int z = 0; z < NumToEmpty; z++)
                    {
                        EmptyGump = OpenRunebook(book);
                        Script.Wait(150);
                        EmptyGump.ClickButton(200);
                        Script.Wait(1000);
                    }
                }
                Console.WriteLine("Moving runes.");
                List<Item> RunesToMove = Self.Backpack.Inventory(0x1F14, false);
                foreach (var rune in RunesToMove)
                {
                    Stealth.Script_DragItem(rune.ID, 1);
                    Script.Wait(500);
                    Stealth.Script_DropItem(WhereRunesGo.ID, (short)WhereRunesGo.X, (short)WhereRunesGo.Y, 0);
                    Script.Wait(1000);
                }
            }

            Self.Backpack.Use();

            Container BagOfBooks = ChooseBag("Target your container of runebooks ...");
            BagOfBooks.Use();
            Container BagofRunes = ChooseBag("Target your container of runes ...");
            BagofRunes.Use();

            Script.Wait(500);

            List<Item> Runebooks = BagOfBooks.Inventory(0x22C5, false);
            List<Item> Runes = BagofRunes.Inventory(0x1F14, false);

            if (Runebooks.Count == 0 || Runes.Count == 0)
                ErrorExit("Not enough resources. Halting.");

            Item MasterRunebook;

            do
            {
                Console.WriteLine("Choose Runebook to clone");
                MasterRunebook = Target.RequestTarget();

                if (MasterRunebook.Type == 0x22C5)
                    break;

                Console.WriteLine("Not a valid runebook.  Try again.");
            } while (true);


            int NumClones = 0;

            while (NumClones < 1 || NumClones > Runebooks.Count)
            {
                Console.Write("How many clones should I make? ");
                NumClones = Convert.ToInt32(Console.ReadLine());

                if (NumClones > Runebooks.Count)
                    Console.WriteLine("You only have {0} runebooks to clone.", Runebooks.Count);
                else if (NumClones < 1)
                    Console.WriteLine("Are you kidding me?!");
            }

            Console.WriteLine("Making/Cloning runebooks ...");

            // 1 open runebook
            // 2 recall each rune
            // 3 mark a rune
            // 4 place marked rune in runebook

            GumpClass RunebookGump = OpenRunebook(MasterRunebook);

            if (RunebookGump == null)
                ErrorExit("Couldn't open master runebook.  Halting.");

            var numRecallButtons = (from x in RunebookGump.XmfHtmlGump
                                    where x.Cliloc_ID == 1062722
                                    select x).Count();


            var numEmptyRunes = (from g in RunebookGump.Textlines()
                                 where g.Equals("Empty")
                                 select g).Count();

            numRecallButtons -= numEmptyRunes;

            if (Runes.Count < numRecallButtons)
                ErrorExit(String.Format("We only have {0} runes left. Halting.", Runes.Count));

            string[] textlines = RunebookGump.Textlines();

            int Button_Offset = 50;

            for (int j = 0; j < numRecallButtons; j++)
            {
                RunebookGump = OpenRunebook(MasterRunebook);
                Script.Wait(1000);

                int ButtonID = Button_Offset + j;

                if (Self.Mana <= 11)
                    while (Self.Mana < Self.MaxMana)
                    {
                        Self.UseSkill("Meditation");
                        Script.Wait(2000);
                    }

                bool ClickResult = RunebookGump.ClickButton(ButtonID);

                if (!ClickResult)
                    ErrorExit("Our click failed. WTF?");

                Script.Wait(4000);

                for (int k = 0; k < NumClones; k++)
                {
                    Item NewRune = Runes[0];
                    Runes.RemoveAt(0);

                    if (Self.Mana <= 20)
                        while (Self.Mana < Self.MaxMana)
                        {
                            Self.UseSkill("Meditation");
                            Script.Wait(2000);
                        }

                    Self.Cast("Mark");
                    Target.WaitForTarget();
                    Target.TargetObject(NewRune);

                    Script.Wait(500);
                    NewRune.Use();
                    Stealth.Script_WaitJournalLine(DateTime.Now, "Please enter a description for this marked object:", 10000);
                    Script.Wait(1000);
                    Stealth.Script_ConsoleEntryUnicodeReply(textlines[2 + j]);
                    Console.WriteLine("We changed the name to {0}", textlines[2 + j]);
                    Script.Wait(1000);

                    Stealth.Script_DragItem(NewRune.ID, 1);
                    Script.Wait(500);
                    Stealth.Script_DropItem(Runebooks[k].ID, (short)Runebooks[k].X, (short)Runebooks[k].Y, 0);
                    Script.Wait(500);
                }
            }
        }
    }
}
